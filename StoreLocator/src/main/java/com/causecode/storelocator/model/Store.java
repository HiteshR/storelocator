package com.causecode.storelocator.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "store")
public class Store implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true)
	private Integer Id;

	@Column(name = "name", nullable = false, length = 20)
	private String name;

	@Column(name = "zipcode", nullable = false)
	private Integer zipCode;

	/**
	 * get id of store table
	 * 
	 * @return id
	 */
	public Integer getId() {
		return Id;
	}

	/**
	 * set id of store
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		Id = id;
	}

	/**
	 * get name of store table
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name of store
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get String of store table
	 * 
	 * @return zipcode
	 */
	public Integer getZipCode() {
		return zipCode;
	}

	/**
	 * set zipcode of store
	 * 
	 * @param zipCode
	 */
	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

}
