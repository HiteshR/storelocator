package com.causecode.storelocator.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.causecode.storelocator.model.Store;
import com.causecode.storelocator.service.LocationService;

/**
 * Handles requests for store crud operation.
 */
@RestController
public class LocationController {

	private static final Logger logger = LoggerFactory.getLogger(LocationController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "home";
	}

	/**
	 * Use location service
	 */
	@Autowired
	@Qualifier("locationService")
	private LocationService locationService;

	/**
	 * get all available store
	 * 
	 * @return list of Store
	 */
	@RequestMapping(value = "/stores", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public List<Store> getAllStore() {
		return locationService.getAllStore();
	}

	/**
	 * add store to database
	 * 
	 * @param store
	 *            contains information about store like name and zipcode
	 * @return success or error message
	 */
	@RequestMapping(value = "/addStore", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public String addStore(@RequestBody Store store) {
		return locationService.addNewStore(store);
	}

	/**
	 * update store data according to id
	 * 
	 * @param storeID
	 *            to update from database
	 * @param store
	 *            data which need to replace
	 * @return success or error message
	 */
	@RequestMapping(value = "/updatestore/{storeID}", method = RequestMethod.PUT, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public String updateStore(@PathVariable Integer storeID, @RequestBody Store store) {
		return locationService.updateStore(storeID, store);
	}

	/**
	 * delete store from database if present
	 * 
	 * @param storeID
	 *            to delete from database
	 * @return success or error message
	 */
	@RequestMapping(value = "/deleteStore/{storeID}", method = RequestMethod.DELETE, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public String deleteStore(@PathVariable Integer storeID) {
		return locationService.deleteStore(storeID);
	}

	/**
	 * return all available store within 'x' mile based on zipcode
	 * 
	 * @param zipcode
	 *            central zipcode
	 * @param mile
	 *            from central zipcode to within radius
	 * @return list of available store within range
	 */
	@RequestMapping(value = "/getStoreByMile/{zipcode}/{mile}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public List<Store> getStoresWithinXmile(@PathVariable Integer zipcode, @PathVariable Integer mile) {
		return locationService.getStoresWithinXmile(zipcode, mile);
	}
}
