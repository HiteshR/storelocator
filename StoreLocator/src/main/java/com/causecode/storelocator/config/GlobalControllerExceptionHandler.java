package com.causecode.storelocator.config;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.causecode.storelocator.dto.ErrorDto;
import com.causecode.storelocator.exceptions.BaseWebApplicationException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

	@ExceptionHandler(BaseWebApplicationException.class)
	@ResponseBody
	public ErrorDto webApplicationException(BaseWebApplicationException baseWebApplicationException,
			HttpServletResponse response) {
		logger.error("webApplicationException", baseWebApplicationException);
		response.setStatus(baseWebApplicationException.getStatus());
		return new ErrorDto(baseWebApplicationException.getErrorMessage(), baseWebApplicationException.getErrorCode(),
				baseWebApplicationException.getDeveloperMessage());
	}
}
