package com.causecode.storelocator.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * all configuration of database
 * 
 * we can use .properties file for DriverClassName, url etc. 
 * 
 * @author HiteshR
 *
 */
@Configuration
@EnableTransactionManagement
public class DatabaseConfig {

	/**
	 * Configure datasource based on database property
	 * 
	 * @return DataSource
	 * @throws PropertyVetoException
	 */
	@Bean
	public ComboPooledDataSource dataSource() throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setDriverClass("com.mysql.jdbc.Driver");
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/storelocator");
		dataSource.setUser("root");
		dataSource.setPassword("root");
		// <!-- these are C3P0 properties -->
		dataSource.setAcquireIncrement(5);
		dataSource.setMaxIdleTime(3600);
		dataSource.setMaxIdleTimeExcessConnections(300);
		dataSource.setMaxPoolSize(100);
		dataSource.setMinPoolSize(20);
		dataSource.setNumHelperThreads(6);
		dataSource.setUnreturnedConnectionTimeout(3600);
		// <!-- Keep pool alive -->
		dataSource.setPreferredTestQuery("SELECT 1");
		dataSource.setTestConnectionOnCheckout(true);
		return dataSource;
	}

	/**
	 * based on datasource and hibernate configuration create sessionfactory
	 * 
	 * @return SessionFactory object
	 * @throws PropertyVetoException
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory() throws PropertyVetoException {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setPackagesToScan(new String[] { "com.causecode.storelocator.model" });
		sessionFactoryBean.setDataSource(dataSource());
		sessionFactoryBean.setHibernateProperties(hibernateProp());
		return sessionFactoryBean;
	}

	/**
	 * hibenate configuration
	 * 
	 * @return Properties contains hibenate configuration
	 */
	private Properties hibernateProp() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		return properties;
	}

	/**
	 * Configuration of Transaction manager based on sessionfactory
	 * 
	 * @param sessionFactory
	 * @return TransacationManager
	 */
	@Bean
	@Autowired
	public HibernateTransactionManager hibernateTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
		hibernateTransactionManager.setSessionFactory(sessionFactory);
		return hibernateTransactionManager;
	}
}
