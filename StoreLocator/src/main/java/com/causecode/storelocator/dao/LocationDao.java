package com.causecode.storelocator.dao;

import java.util.List;

import com.causecode.storelocator.model.Store;
import com.causecode.storelocator.model.ZipCode;

public interface LocationDao {
	public List<Store> getAllStore();

	public void addNewStore(Store store);

	public void updateStore(Store store);

	public void deleteStore(Store store);

	Store getStore(Integer storeID);

	ZipCode getZipCode(Integer zipcode);

	List<Store> getStoresWithinXmile(Integer zipcode, Integer mile);
}
