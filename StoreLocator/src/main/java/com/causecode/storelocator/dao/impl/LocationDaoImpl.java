package com.causecode.storelocator.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.causecode.storelocator.dao.LocationDao;
import com.causecode.storelocator.model.Store;
import com.causecode.storelocator.model.ZipCode;

@Repository(value = "locationDao")
public class LocationDaoImpl implements LocationDao {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * get store from database using sessionfactory
	 * 
	 */
	@Override
	public List<Store> getAllStore() {
		return sessionFactory.getCurrentSession().createCriteria(Store.class).list();
	}

	@Override
	public Store getStore(Integer storeID) {
		return (Store) sessionFactory.getCurrentSession().get(Store.class, storeID);
	}

	@Override
	public void addNewStore(Store store) {
		sessionFactory.getCurrentSession().save(store);
	}

	@Override
	public void updateStore(Store store) {
		sessionFactory.getCurrentSession().update(store);
	}

	@Override
	public void deleteStore(Store store) {
		sessionFactory.getCurrentSession().delete(store);
	}

	/**
	 * get ZipCode class object based on provided zipcode
	 * 
	 * @return ZipCode
	 */
	@Override
	public ZipCode getZipCode(Integer zipcode) {
		return (ZipCode) sessionFactory.getCurrentSession().get(ZipCode.class, zipcode);
	}

	/**
	 * get all stores within 'X' mile of provided zipcode
	 * 
	 * this query I used for my previous project where we
	 * need to find person within X range from available location
	 */
	@Override
	public List<Store> getStoresWithinXmile(Integer zipcode, Integer mile) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("SELECT store.*").append(" FROM store AS store ")
				.append(" INNER JOIN zipcodes AS zipcodes ON `store`.zipcode=`zipcodes`.`zipcode`")
				.append(" WHERE (SELECT ( 3959 ").append("* ACOS(").append("	COS( RADIANS( zip.latitude ) )")
				.append(" * COS( RADIANS( zipcodes.latitude ) )")
				.append(" * COS( RADIANS( zipcodes.longitude ) - RADIANS( zip.longitude ) )")
				.append(" + SIN( RADIANS( zip.latitude ) ) ").append(" * SIN( RADIANS( zipcodes.latitude ) )")
				.append(" ) ").append(")  FROM zipcodes AS zip WHERE zip.`zipcode`=:zipcode) <:mile");
		return sessionFactory.getCurrentSession().createSQLQuery(stringBuilder.toString())
				.setParameter("zipcode", zipcode).setParameter("mile", mile).list();
	}
}
