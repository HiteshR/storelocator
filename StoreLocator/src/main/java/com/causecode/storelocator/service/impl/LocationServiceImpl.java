package com.causecode.storelocator.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.causecode.storelocator.dao.LocationDao;
import com.causecode.storelocator.exceptions.NotFoundException;
import com.causecode.storelocator.exceptions.NotFoundException.NotFound;
import com.causecode.storelocator.model.Store;
import com.causecode.storelocator.model.ZipCode;
import com.causecode.storelocator.service.LocationService;

@Service(value = "locationService")
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationDao locationDao;

	/**
	 * return all available store from database
	 * 
	 */
	@Override
	@Transactional
	public List<Store> getAllStore() {
		return locationDao.getAllStore();
	}

	/**
	 * add new store to database also check zipcode is present in database
	 * 
	 * @return success or error message
	 */
	@Override
	@Transactional
	public String addNewStore(Store store) {
		ZipCode zipCode = locationDao.getZipCode(store.getZipCode());
		if (zipCode != null) {
			locationDao.addNewStore(store);
			return "success";
		} else {
			throw new NotFoundException(NotFound.StoreNotFound);
		}
	}

	/**
	 * update store of database also check store and zipcode is present in
	 * database
	 * 
	 * @return success or error message
	 */
	@Override
	@Transactional
	public String updateStore(Integer storeID, Store store) {
		ZipCode zipCode = locationDao.getZipCode(store.getZipCode());
		if (zipCode != null) {
			Store storeLoded = locationDao.getStore(storeID);
			if (storeLoded != null) {
				store.setId(storeID);
				locationDao.updateStore(store);
				return "success";
			} else {
				throw new NotFoundException(NotFound.StoreNotFound);
			}
		} else {
			throw new NotFoundException(NotFound.ZipNotFound);
		}
	}

	/**
	 * delete store from database if store is present in database
	 * 
	 * @return success or error message
	 */
	@Override
	@Transactional
	public String deleteStore(Integer storeID) {
		Store store = locationDao.getStore(storeID);
		if (store != null) {
			locationDao.deleteStore(store);
			return "success";
		} else {
			throw new NotFoundException(NotFound.StoreNotFound);
		}
	}

	/**
	 * get all store within 'X' mile of given zipcode
	 * 
	 * @return all available location within 'X' mile
	 */
	@Override
	@Transactional
	public List<Store> getStoresWithinXmile(Integer zipcode, Integer mile) {
		return locationDao.getStoresWithinXmile(zipcode, mile);
	}
}
