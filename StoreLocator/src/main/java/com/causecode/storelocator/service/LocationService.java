package com.causecode.storelocator.service;

import java.util.List;

import com.causecode.storelocator.model.Store;

public interface LocationService {
	public List<Store> getAllStore();

	public String addNewStore(Store store);

	public String updateStore(Integer storeID, Store store);

	public String deleteStore(Integer storeId);

	List<Store> getStoresWithinXmile(Integer zipcode, Integer mile);
}
