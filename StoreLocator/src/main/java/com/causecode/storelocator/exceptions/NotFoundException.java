package com.causecode.storelocator.exceptions;

public class NotFoundException extends BaseWebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4553101964302123808L;

	public NotFoundException(NotFound notFound) {
		super(404, notFound.getStatusCode(), notFound.getErrorMessage(), notFound.getDeveloperMessage());
	}

	public enum NotFound {
		StoreNotFound("40401", "Store Not Found", "No Store could be found for that Id"), ZipNotFound("40402",
				"zipcode Not Found", "No such zipcode present in current database");

		private String statusCode;
		private String errorMessage;
		private String developerMessage;

		private NotFound(String statusCode, String errorMessage, String developerMessage) {
			this.statusCode = statusCode;
			this.errorMessage = errorMessage;
			this.developerMessage = developerMessage;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public String getDeveloperMessage() {
			return developerMessage;
		}

	}
}
