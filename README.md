store locator is for add new store, update, delete search within 'X' mile of zipcode

### How do I get set up? ###

* clone project 
* import as maven project from eclise
* run on server like tomcate (As I check with Pivotal tc server)

### Database setup ###
* import and execute "**storelocator.sql**" in mysql

### Error code ###

* 40401 - store not found
* 40402 - zipcode not found

### Api call setup ###
* add postman or rest client to browser
* set Content-Type to application/json for each request

## Add new store ##
* prepare data for add **{"name":"Amazon","zipCode":"35005"}** to body 
* hit url **{{localhost}}/addStore**
* method must be POST while hit url

## update store ##
* prepare data for add **{"name":"Amazon","zipCode":"35005"}** to body 
* hit url **{{localhost}}/updatestore/{id}**
* method must be PUT while hit url
* here {id} need to replace with database store table id

## delete store ##
* hit url **{{localhost}}/deleteStore/{id}**
* method must be DELETE while hit url
* here {id} need to replace with database store table id

## get all store ##
* hit url **{{localhost}}/stores**
* method must be GET while hit url

## get all store within 'X' mile of zipcode##
* hit url **{{localhost}}/getStoreByMile/{zipcode}/{mile}**
* method must be GET while hit url
* here {zipcode} need to replace with us zipcode and {mile} need to replace with mile within which you need to search for store

Note: you need to replace {{localhost}} according to your server configuration in my case it is **http://localhost:8080/storelocator**